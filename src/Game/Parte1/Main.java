package Game.Parte1;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Main {
	
	private static Field f = new Field();
	private static Window w = new Window(f);
	
	public static void main(String[] args) throws InterruptedException {
		boolean inGame = true;
		Roca roca1 = new Roca();
		Roca roca2 = new Roca(0, 100, 50);
		Roca roca3 = new Roca(0, 200, 50, 250, 50);
		Roca roca4 = new Roca("Roca4", 100, 0, 150, 50, 0, "res/b84.png");
		Roca roca5 = new Roca("Roca5", 200, 0, 250, 50, 0, "res/b84.png", 50);
		
		while(inGame) {
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(roca1);
			sprites.add(roca2);
			sprites.add(roca3);
			sprites.add(roca4);
			sprites.add(roca5);
			f.draw(sprites);
			Thread.sleep(30);
		}
	}

}
