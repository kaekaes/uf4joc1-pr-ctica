package Game.Parte4.Enemics;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;

public class Spawner{
	
	static int velY = 3;
	static Field f;
	static int speedIncreaseTime = 15*1000;
	static int cooldown = 1000;
	
	public Spawner(Field f) {
		Spawner.f = f;
		timer.schedule(new TimerTaskSpawner(), cooldown);
		timer.schedule(increaseSpawnTask, 0, speedIncreaseTime);
	}
	
	private static void generarEnemic() {
		EnemicFactory ef = new EnemicFactory();
		int x = new Random().nextInt(f.getWidth()-75)+32;
		ef.generateEnemic(x, -50, velY, f);
	}
	
	static Timer timer = new Timer();
	private class TimerTaskSpawner extends TimerTask { 		
		@Override
		public void run(){
			generarEnemic();
			cooldown = cooldown-2>=60?cooldown-2:cooldown;
			timer.schedule(new TimerTaskSpawner(), cooldown);
		}
	};

	static TimerTask increaseSpawnTask = new TimerTask(){
		@Override
		public void run(){
			velY++;
		}
	};
}
