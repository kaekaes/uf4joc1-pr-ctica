package Game.Parte4.Enemics;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Game.Parte4.Buff;
import Game.Parte4.BuffType;
import Game.Parte4.Nau;
import Game.Parte4.Proyectil;
import Game.Parte4.UIs.UI;

public class EnemicDisparador extends PhysicBody implements Enemic{

	private int timeToShoot = 1750;
	private Sprite instance;
	private int velY;

	public EnemicDisparador(int x1, int y1, int velY, Field f){
		super("enemy", x1 - (width / 2), y1 - (height / 2), x1 + (width / 2), y1 + (width / 2), 0, "res/yellow.png", f);
		this.trigger = true;
		this.velY = (int) (velY/1.25f);
		setVelocity(0, this.velY);
		instance = this;
		timer.schedule(taskDisparar, 0, timeToShoot);
	}

	@Override
	public void update(){
		if(this.y1 > this.f.getHeight())
			this.delete();
	}

	Timer timer = new Timer();
	TimerTask taskDisparar = new TimerTask(){
		@Override
		public void run(){
			if(delete) 
				cancel();
			else
				new Proyectil(instance, "res/mandarina.png", f, true, velY);
		}
	};

	// {{ colls

	@Override
	public void onTriggerEnter(Sprite sprite){
		if(sprite instanceof Proyectil){
			Proyectil p = (Proyectil) sprite;
			if(!p.imAmBad){
				sprite.delete();
				this.delete();
				UI.getInstance().score.addScore(100);

				Random r = new Random();
				if(r.nextInt(8) == 0)
					new Buff(BuffType.values()[r.nextInt(BuffType.values().length)], this.x1, this.y2, f);
				
			}
		}
		if(sprite instanceof Nau){
			Nau.hit();
			this.delete();
		}
	}

	@Override
	public void onCollisionEnter(Sprite sprite){
		// TODO Auto-generated method stub
	}

	@Override
	public void onCollisionExit(Sprite sprite){
		// TODO Auto-generated method stub
	}
	// }}
}
