package Game.Parte4.UIs;

import java.awt.Font;

import Core.Field;
import Core.Sprite;

public class Score extends Sprite implements UIComponent{

	private int score = 0;

	public Score(int x1, int y1, Field f){
		super("Puntuación", x1, y1, x1, y1, 0, "", f);
		this.text = true;
		this.solid = false;
		this.font = new Font("Monospaced", Font.BOLD, 40);
		this.textColor = 0xFFFFFF;
		addScore(0);
	}

	public void addScore(int score){
		this.score += score;
		this.path = "Score: " + this.score;
	}

	public int getScore(){
		return score;
	}

}
