package Game.Parte4;

import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Game.Parte4.UIs.Fogeo;
import Game.Parte4.UIs.HP;
import Game.Parte4.UIs.UI;

public class Buff extends PhysicBody{

	static int size = 32;
	BuffType bt;

	int buffedTime = 5000;

	public Buff(BuffType bt, int x1, int y1, Field f){
		super(bt.toString(), x1 - (size / 2), y1 - (size / 2), x1 + (size / 2), y1 + (size / 2), 0, "", f);
		trigger = true;
		this.bt = bt;
		switch (bt){
		case SPEED:
			this.path = "res/buff_speed.png";
			break;
		case MULTISHOOT:
			this.path = "res/buff_multishoot.png";
			break;
		case SPEEDSHOOTING:
			this.path = "res/buff_speedShooting.png";
			break;
		case EXTRALIFE:
			this.path = "res/buff_extraLife.png";
			break;
		case EXTRAFOGEO:
			this.path = "res/buff_extraFogeo.png";
			break;
		}

		setVelocity(0, 4);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update(){
		if(this.y1 > this.f.getHeight())
			delete();
	}

	// {{ coll
	@Override
	public void onTriggerEnter(Sprite sprite){
		if(sprite instanceof Nau){
			switch (this.bt){
			case EXTRALIFE:
				if(Nau.vida >= 9){
					UI.getInstance().score.addScore(500);
				} else{
					Nau.vida++;
					UI.getInstance().vidas.push(new HP((int) ((20 + (Nau.vida * Nau.width * 1.5)) - 50),
							UI.getInstance().vidas.peek().y1, f));
				}
				break;
			case EXTRAFOGEO:
				if(Nau.fogeos >= 3){
					UI.getInstance().score.addScore(500);
				} else{
					Nau.fogeos++;
					UI.getInstance().fogeos
							.add(new Fogeo((int) (f.getWidth() - ((20 + (Nau.fogeos * Nau.width * 1.5)) - 10)),
									UI.getInstance().vidas.peek().y1 - 1, f));
				}
				break;

			case SPEED:
				if(Nau.runningBuff)
					UI.getInstance().score.addScore(250);
				else
					Nau.runningBuff = true;
				timer.schedule(addSpeedTask, buffedTime);
				break;
			case MULTISHOOT:
				if(Nau.multipleShootingBuff)
					UI.getInstance().score.addScore(250);
				else
					Nau.multipleShootingBuff = true;
				timer.schedule(multiShootingTask, buffedTime);
				break;
			case SPEEDSHOOTING:
				if(Nau.speedShootingBuff)
					UI.getInstance().score.addScore(250);
				else
					Nau.speedShootingBuff = true;
				timer.schedule(addSpeedShootingTask, buffedTime);
				break;
			default:
				break;
			}
			this.delete();
		}
	}

	@Override
	public void onCollisionEnter(Sprite sprite){
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(Sprite sprite){
		// TODO Auto-generated method stub

	}
	// }}

	static Timer timer = new Timer();
	TimerTask addSpeedTask = new TimerTask(){
		@Override
		public void run(){
			Nau.runningBuff = false;
		}
	};
	TimerTask addSpeedShootingTask = new TimerTask(){
		@Override
		public void run(){
			Nau.speedShootingBuff = false;
		}
	};
	TimerTask multiShootingTask = new TimerTask(){
		@Override
		public void run(){
			Nau.multipleShootingBuff = false;
		}
	};
}