package Game.Parte3;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;
import Game.Parte3.Inputs;
import Game.Parte2.Plataforma;

public class Main {
	
	public final static float GRAVITYFORCE = 1.9f;
	public static Field f = new Field();
	public static Window w = new Window(f);

	public static ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	private static Personaje pj = new Personaje("Pj", -w.getWidth()+100, 50, "res/b84.png");

	public static void main(String[] args) throws InterruptedException {
		
		boolean inGame = true;
		w.setResizable(false);
		
		addSprites();
		while(inGame) {
			input(pj);
			f.draw(sprites);
			Thread.sleep(30);
		}
	}
	
	private static void addSprites() {
		sprites.add(new Plataforma(-f.getWidth(), f.getHeight()-20, f.getWidth()*3, 20, "res/black.png"));		
		sprites.add(new Plataforma(-f.getWidth(), -f.getHeight(), 20, f.getHeight()*2, "res/black.png"));		
		sprites.add(new Plataforma(f.getWidth()*2, -f.getHeight(), 20, f.getHeight()*2, "res/black.png"));		
		sprites.add(new Plataforma(-f.getWidth()+250, 300, 20, 50, "res/black.png"));		
		sprites.add(new Obstacle(-450, 230, 50, 75, "res/miVida2.png"));
		sprites.add(new Enemic(-250, 50, 3));
		sprites.add(new Obstacle(200, 270, 50, 75, "res/miVida2.png"));		
		sprites.add(new Enemic(300, 50, 3));
		sprites.add(pj);
	}

	private static void input(Personaje pj) {		
		if(w.getPressedKeys().contains('w') || w.getPressedKeys().contains(' ')) {
			pj.moviment(Inputs.SALTO);
		}
		
		if(w.getKeysDown().contains('j')) {
			pj.shoot();
		}
		
		if(w.getPressedKeys().contains('a')) {
			pj.moviment(Inputs.IZQUIERDA);
			pj.lookingRight = false;
		}else if(w.getPressedKeys().contains('d')) {
			pj.moviment(Inputs.DERERCHA);
			pj.lookingRight = true;
		}else {
			pj.moviment(Inputs.QUIETO);
		}
	}
}
