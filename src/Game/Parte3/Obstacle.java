package Game.Parte3;

import Core.PhysicBody;
import Core.Sprite;

public class Obstacle extends PhysicBody implements Disparable{

	static int obstCounter = 0;
	
	public Obstacle(int x1, int y1, int width, int height, String path) {
		super("Obs"+obstCounter, x1, y1, x1+width, y1+height, 0, path);
		obstCounter++;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void danyar() {
		this.delete();
		
	}

}
